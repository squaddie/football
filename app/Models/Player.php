<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    protected $table = 'players';
    protected $fillable = [
        'team_id',
        'name',
        'role',
        'nationality',
        'number',
        'clean_sheets',
        'appearances',
    ];
}
