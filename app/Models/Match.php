<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Http\Request;

class Match extends Model
{
    use HasFactory;

    protected $table = 'matches';
    protected $fillable = [
        'week',
        'played',
        'winner'
    ];

    /*
     * Helpers
     */

    public function getTeamGoals($team)
    {
        $goals = $this->results()->pluck('goals','team_id', );

        return $goals[$team];
    }

    /*
     * Relations
     */

    public function couples(): HasMany
    {
        return $this->hasMany(Couple::class, 'match_id', 'id');
    }

    public function teams(): HasManyThrough
    {
        return $this->hasManyThrough(Team::class,Couple::class,'match_id','id','id','team_id');
    }

    public function results(): HasMany
    {
        return $this->hasMany(Result::class, 'match_id', 'id');
    }

    /*
     * Scopes
     */

    public function scopeWeekFilter($query, Request $request)
    {
        if ($request->get('week') <= 3) {
            return $query->where('week', $request->get('week') - 1);
        }

        return $query;
    }
}
