<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $table = 'teams';
    protected $fillable = [
        'team'
    ];

    public function prediction()
    {
        $teamss = $this->pluck('team', 'id');
        $result = Result::all();
        $percent100 = $result->sum('points');

        $t = [];
        $teams = $result->groupBy('team_id');
        foreach ($teams as $key => $team) {
            $t[$key]['points'] = $team->sum('points');
            $t[$key]['team'] = $key;
        }

        $r = [];

        foreach ($t as $key => $item) {
            $r[$key]['team'] = $teamss[$item['team']];
            $r[$key]['percent'] = $item['points'] != 0 ? round(($item['points'] * 100) / $percent100, 0) : 0;
        }

        usort($r, function ($a, $b) {
            if ($a['percent'] == $b['percent']) {
                return 0;
            }
            return ($a['percent'] < $b['percent']) ? 1 : -1;
        });


        return $r;

    }

    public function getRandomTeams($limit = 4): array
    {
        $limit = ($limit % 2 !== 0) ? $limit + 1 : $limit;

        return $this->query()->inRandomOrder()->limit($limit)->pluck('id')->toArray();
    }
}
