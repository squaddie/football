<?php


namespace App\Services;


use App\Models\Match;
use App\Models\Team;

class LeagueBuilder
{
    public $matchModel;

    public function __construct()
    {
        $this->matchModel = new Match();
    }

    /**
     * Round robin implementation
     *
     * @param $teams
     * @return array
     */
    public function scheduler($teams): array
    {
        if (count($teams) % 2 !== 0)
            exit();

        $match = [];
        $away = array_splice($teams, (count($teams) / 2));
        $home = $teams;
        for ($i = 0; $i < count($home) + count($away) - 1; $i++) {
            for ($j = 0; $j < count($home); $j++) {
                $match[$i][$j]["home"] = $home[$j];
                $match[$i][$j]["away"] = $away[$j];
            }
            if (count($home) + count($away) - 1 > 2) {
                $array = array_splice($home, 1, 1);
                array_unshift($away, array_shift($array));
                array_push($home, array_pop($away));
            }
        }

        return $match;
    }

    public function prepareScheduledData($schedule): array
    {
        $matches = [];
        foreach ($schedule as $round => $games) {
            foreach ($games as $play) {
                $matches[] = [
                    'home' => $play["home"],
                    'away' => $play["away"],
                    'week' => $round + 1
                ];
            }
        }

        return $matches;
    }

    public function getRandomTeams(): array
    {
        $team = new Team();
        return $team->getRandomTeams();
    }

    public function fillDatabase($data)
    {
        foreach ($data as $datum) {
            $match = $this->matchModel->create(['week' => $datum['week']]);
            $match->couples()->create(['team_id' => $datum['home']]);
            $match->couples()->create(['team_id' => $datum['away']]);
        }
    }

    public function build()
    {
        $teams =  $this->getRandomTeams();
        $scheduler = $this->scheduler($teams);
        $data = $this->prepareScheduledData($scheduler);
        $this->fillDatabase($data);
    }


    public function getResult()
    {
        return Match::with('couples', 'teams')->where('week', request()->get('open_week', 1))->get();
    }
}
