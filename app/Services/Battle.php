<?php


namespace App\Services;


use App\Models\Match;
use Illuminate\Http\Request;

class Battle
{
    const winPoints = 3;
    const drawPoints = 1;
    const losePoints = 0;

    public $match;

    public function __construct()
    {
        $this->match = new Match();
    }

    public function goalChance(): int
    {
        return mt_rand(0, 100);
    }

    public function protectChance(): int
    {
        return mt_rand(0, 100);
    }

    public function attack(): bool
    {
        return $this->goalChance() < $this->protectChance();
    }

    public function getMatches($week)
    {
        return $this->match->with('teams')->where('week', $week)->limit(2)->latest()->get();
    }

    public function getAttacksNumber(): int
    {
        return mt_rand(0, 10);
    }

    public function detectWinner($battle)
    {
        $result = [];
        if (mt_rand(0, 10) == mt_rand(1, 11))
            return false; // Make draw sometimes

        if ($battle[0]['goals'] > $battle[1]['goals']) {
            $result['id'] = $battle[0]['id'];
            $result['goals'] = $battle[0]['goals'];
        } else if ($battle[0]['goals'] < $battle[1]['goals']) {
            $result['id'] = $battle[1]['id'];
            $result['goals'] = $battle[1]['goals'];
        } else
            return false; // Make draw

        return $result;
    }

    public function detectLoser($winner, $data): array
    {
        foreach ($data as $datum) {
            if ($datum['id'] !== $winner['id']) {
                return [
                    'id' => $datum['id'],
                    'goals' => $datum['goals'],
                ];
            }
        }

        return [];
    }


    public function play($match): array
    {

        $teamsIdentify = $match->teams->pluck('id')->toArray();
        $battle[0]['id'] = $teamsIdentify[0];
        $battle[0]['attacks'] = $this->getAttacksNumber();
        $battle[0]['goals'] = 0;
        $battle[1]['id'] = $teamsIdentify[1];
        $battle[1]['attacks'] = $this->getAttacksNumber();
        $battle[1]['goals'] = 0;

        for ($i = 1; $i <mt_rand($i, 10); $i++) {
            if ($this->attack()) {
                $battle[0]['goals'] =+ 1;
            }
        }
        for ($i = 1; $i < mt_rand($i, 10); $i++) {
            if ($this->attack()) {
                $battle[1]['goals'] += 1;
            }
        }


        $winner = $this->detectWinner($battle);
        if ($winner) {
            $loser = $this->detectLoser($winner, $battle);
            return [
                'winner' => $winner,
                'loser' => $loser,
            ];
        }

        return $teamsIdentify;
    }

    public function closeMatch($data)
    {
        $match = $this->match->find($data['match']);
        $update = ['played' => true];
        $battle = $data['battle'];
        if (isset($data['battle']['winner'])) {
            $match->results()->create([
                'status' => 'winner',
                'team_id' => $battle['winner']['id'],
                'goals' => $battle['winner']['goals'],
                'points' => self::winPoints,
            ]);
            $match->results()->create([
                'status' => 'loser',
                'team_id' => $battle['loser']['id'],
                'goals' => $battle['loser']['goals'],
                'points' => self::losePoints,
            ]);

            $update['winner'] = $battle['winner']['id'];
        } else {
            $match->results()->create([
                'status' => 'draw',
                'team_id' => $battle[0],
                'goals' => 0,
                'points' => self::drawPoints,
            ]);
            $match->results()->create([
                'status' => 'draw',
                'team_id' => $battle[1],
                'goals' => 0,
                'points' => self::drawPoints,
            ]);
        }

        $match->update($update);
    }

    public function run(Request $request)
    {
        $matches = $this->getMatches($request->get('play_week'));
        foreach ($matches as $key => $match) {
            $battle = $this->play($match);
            $results['battle'] = $battle;
            $results['match'] = $match->id;

            $this->closeMatch($results);
            unset($results);
        }
    }
}
