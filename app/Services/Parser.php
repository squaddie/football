<?php


namespace App\Services;


use Illuminate\Support\Facades\DB;
use PHPHtmlParser\Dom;

class Parser
{
    public $links = [
        [
            'team_id' => 1,
            'link' => 'https://www.premierleague.com/clubs/11/Manchester-City/squad'
        ],
        [
            'team_id' => 2,
            'link' => 'https://www.premierleague.com/clubs/12/Manchester-United/squad'
        ],
        [
            'team_id' => 3,
            'link' => 'https://www.premierleague.com/clubs/26/Leicester-City/squad'
        ],
        [
            'team_id' => 4,
            'link' => 'https://www.premierleague.com/clubs/4/Chelsea/squad'
        ],
        [
            'team_id' => 5,
            'link' => 'https://www.premierleague.com/clubs/25/West-Ham-United/squad'
        ],
        [
            'team_id' => 6,
            'link' => 'https://www.premierleague.com/clubs/21/Tottenham-Hotspur/squad'
        ],
        [
            'team_id' => 7,
            'link' => 'https://www.premierleague.com/clubs/10/Liverpool/squad'
        ],
        [
            'team_id' => 8,
            'link' => 'https://www.premierleague.com/clubs/7/Everton/squad'
        ],
        [
            'team_id' => 9,
            'link' => 'https://www.premierleague.com/clubs/1/Arsenal/squad'
        ],
        [
            'team_id' => 10,
            'link' => 'https://www.premierleague.com/clubs/2/Aston-Villa/squad'
        ],
        [
            'team_id' => 11,
            'link' => 'https://www.premierleague.com/clubs/9/Leeds-United/squad'
        ],
        [
            'team_id' => 12,
            'link' => 'https://www.premierleague.com/clubs/6/Crystal-Palace/squad'
        ],
        [
            'team_id' => 13,
            'link' => 'https://www.premierleague.com/clubs/38/Wolverhampton-Wanderers/squad'
        ],
        [
            'team_id' => 14,
            'link' => 'https://www.premierleague.com/clubs/20/Southampton/squad'
        ],
        [
            'team_id' => 15,
            'link' => 'https://www.premierleague.com/clubs/43/Burnley/squad'
        ],
        [
            'team_id' => 16,
            'link' => 'https://www.premierleague.com/clubs/131/Brighton-and-Hove-Albion/squad'
        ],
        [
            'team_id' => 17,
            'link' => 'https://www.premierleague.com/clubs/23/Newcastle-United/squad'
        ],
        [
            'team_id' => 18,
            'link' => 'https://www.premierleague.com/clubs/34/Fulham/squad'
        ],
        [
            'team_id' => 19,
            'link' => 'https://www.premierleague.com/clubs/36/West-Bromwich-Albion/squad'
        ],
        [
            'team_id' => 20,
            'link' => 'https://www.premierleague.com/clubs/18/Sheffield-United/squad'
        ],
    ];

    public $parser;

    public function __construct()
    {
        $this->parser = new Dom();
    }

    public function getSources(): array
    {
        return $this->links;
    }

    public function parseLink(string $link): ?Dom\Node\Collection
    {
        $this->parser->loadFromUrl($link);

        return $this->parser->find('.playerOverviewCard');
    }

    public function parseCard($cards)
    {
        $players = [];
        foreach ($cards as $key => $card) {
            $number = $card->find('.number')[0]->innerHtml;
            $position = $card->find('.position')[0]->innerHtml;
            $name = $card->find('.name')[0]->innerHtml;
            $country = $card->find('.playerCountry')[0]->innerHtml;

            echo "$country <br>";

            if ($number !== '-') {
                $players[$key]['name'] = $name;
                $players[$key]['role'] = $position;
                $players[$key]['nationality'] = $country !== '' ? $country : '-';
                $players[$key]['number'] = $number;
            }
        }

        return $players;

    }

    public function modifyResult($teamId, $data): array
    {
        return array_map(function ($array) use ($teamId) {
            $array['team_id'] = $teamId;
            $array['created_at'] = now();
            $array['clean_sheets'] = mt_rand(0, 55);
            $array['appearances'] = mt_rand(0, 20);

            return $array;
        }, $data);
    }

    public function fillDatabase($data)
    {
        DB::table('players')->insert($data);
    }

    public function run()
    {
        foreach ($this->getSources() as $source) {
            $cards = $this->parseLink($source['link']);
            $players = $this->parseCard($cards);
            $result = $this->modifyResult($source['team_id'], $players);

            $this->fillDatabase($result);
        }
    }


}
