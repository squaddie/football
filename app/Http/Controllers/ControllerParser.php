<?php

namespace App\Http\Controllers;

use App\Services\Parser;
use Illuminate\Support\Facades\DB;

class ControllerParser extends Controller
{
    public function index(Parser $parser)
    {
        $tables = DB::select('SHOW TABLES');
        foreach (json_decode(json_encode($tables), true) as $table)
            DB::table(end($table))->truncate();

        $parser->run();

        return redirect()->route('home');
    }
}
