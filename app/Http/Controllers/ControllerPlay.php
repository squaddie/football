<?php

namespace App\Http\Controllers;

use App\Models\Match;
use App\Models\Team;
use App\Services\Battle;
use App\Services\LeagueBuilder;
use Illuminate\Http\Request;

class ControllerPlay extends Controller
{
    public $play;

    public function __construct()
    {
        $this->play = new Battle();
    }

    public function play(Request $request)
    {
        $playWeek = $request->get('play_week') + 1;
        $openWeek = $request->get('open_week') + 1;
        $this->play->run($request);

        return response()->json(['play_week' => $playWeek, 'open_week' => $openWeek]);
    }

    public function getLeagueTable(LeagueBuilder $builder)
    {
        return view('ajax.league', [
            'league' => $builder->getResult(),
        ]);
    }

    public function getResultTable(Request $request, Match $match)
    {
        return view('ajax.results', [
            'results' => $match->weekFilter($request)->get()
        ]);
    }

    public function getPredictionTable(Request $request, Team $team)
    {
        return view('ajax.prediction', [
            'teams' => $team,
            'week' => $request->get('week')
        ]);
    }
}
