<?php

namespace App\Http\Controllers;

use App\Models\Match;
use App\Models\Result;
use App\Models\Team;
use App\Services\LeagueBuilder;
use Illuminate\Support\Facades\DB;

class ControllerHome extends Controller
{
    public function index(LeagueBuilder $builder, Match $matches, Team $team)
    {
        DB::table('matches')->truncate();
        DB::table('results')->truncate();
        DB::table('couples')->truncate();
        $builder->build();


        return view('main.index', [
            'league' => $builder->getResult(),
        ]);
    }
}
