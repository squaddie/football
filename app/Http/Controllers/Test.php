<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Services\Parser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPHtmlParser\Dom;

class Test extends Controller
{
    const WIN_POINTS = 3;
    const DRAW_POINTS = 1;
    const LOSE_POINTS = 0;

    protected $clubsSource = [
        [
            'id' => 1,
            'team' => 'Manchester City',
        ],
        [
            'id' => 2,
            'team' => 'Manchester United',
        ],
        [
            'id' => 3,
            'team' => 'Leicester City',
        ],
        [
            'id' => 4,
            'team' => 'Chelsea',
        ],
        [
            'id' => 5,
            'team' => 'Everton',
        ],
        [
            'id' => 6,
            'team' => 'Leeds United',
        ],
        [
            'id' => 7,
            'team' => 'Burnley',
        ],
        [
            'id' => 8,
            'team' => 'Fulham'
        ],


    ];

    public function getRandomClubs(): array
    {
        $clubs = $this->clubsSource;
        shuffle($clubs);
        return array_slice($clubs, 0, 4);
    }

    function scheduler($teams)
    {
        if (count($teams) % 2 != 0) {
            array_push($teams, "bye");
        }
        $away = array_splice($teams, (count($teams) / 2));
        $home = $teams;
        for ($i = 0; $i < count($home) + count($away) - 1; $i++) {
            for ($j = 0; $j < count($home); $j++) {
                $round[$i][$j]["Home"] = $home[$j];
                $round[$i][$j]["Away"] = $away[$j];
            }
            if (count($home) + count($away) - 1 > 2) {
                $array = array_splice($home, 1, 1);
                array_unshift($away, array_shift($array));
                array_push($home, array_pop($away));
            }
        }

        return $round;
    }

    public function index()
    {

        return view('main.index');



        exit();
        $team = new Team();
        $teams = $team->getRandomTeams();


        $members = $teams;
        $schedule = $this->scheduler($members);

        $matches = [];
        foreach ($schedule as $round => $games) {
            echo "Round: " . ($round + 1) . "<br>";
            foreach ($games as $play) {
                $matches[] = ['team_one_id' => $play["Home"], 'team_two_id' => $play["Away"], 'week' => $round + 1];
                echo $play["Home"] . " - " . $play["Away"] . "<br>";
            }
            echo "<br>";
        }

        DB::table('matches')->insert($matches);

        dd($matches);

        exit();


        $weeks = 5;


        $clubs = $this->getRandomClubs();

        $weeks = [
            1 => [
                1, 2
            ],
            2 => [
                2, 1
            ],
            3 => [
                3, 4
            ],
            4 => [
                4, 3
            ]
        ];

        $i = 0;
        foreach ($weeks as $week) {
            while ($i < 10) {
                shuffle($week);
                $i++;
            }
            dd($week);
        }


    }

    public function test()
    {
        return 11;
    }
}
