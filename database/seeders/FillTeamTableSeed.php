<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FillTeamTableSeed extends Seeder
{
    public function getTeamsFile(): string
    {
        return file_get_contents(public_path('sources/teams.txt'));
    }

    public function mutateArray($data): array
    {
        return array_map(fn($row) => ['team' => $row, 'created_at' => now()], $data);
    }

    public function parseTeams(): array
    {
        $file = $this->getTeamsFile();
        $teams = explode(',', $file);
        $teams = array_filter($teams); // Check array on empty / null values and cut them

        return $teams;
    }

    public function fillTable()
    {
        $teams = $this->parseTeams();
        $correctData = $this->mutateArray($teams);
        DB::table('teams')->insert($correctData);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->fillTable();
    }
}
