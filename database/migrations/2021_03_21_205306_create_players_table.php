<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('team_id');
            $table->string('name');
            $table->string('role');
            $table->string('nationality');
            $table->unsignedTinyInteger('number');
            $table->unsignedSmallInteger('clean_sheets')->default(0);
            $table->unsignedSmallInteger('appearances')->default(0);
            $table->timestamps();

            $table->index('role');
            $table->index('nationality');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
