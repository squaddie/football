@if($league->count() === 0)
    <p><b>League is over</b></p>
@else
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th>Teams</th>
        <th>PTS</th>
        <th>P</th>
        <th>W</th>
        <th>D</th>
        <th>L</th>
        <th>GD</th>
    </tr>
    </thead>
    <tbody>
    @foreach($league as $team)
        @foreach($team->teams as $data)
            <tr>
                <td>{{ $data->team }}</td>
                <td>{{ mt_rand(0,15) }}</td>
                <td>{{ mt_rand(0,15) }}</td>
                <td>{{ mt_rand(0,15) }}</td>
                <td>{{ mt_rand(0,15) }}</td>
                <td>{{ mt_rand(0,15) }}</td>
                <td>{{ mt_rand(0,15) }}</td>
            </tr>
        @endforeach
    @endforeach
    <tr>
        <td colspan="4">
            <button class="play-all" type="button">Play all</button>
        </td>
        <td colspan="3">
            <button class="play" type="button">Next week</button>
        </td>
    </tr>
    </tbody>
</table>
@endif
