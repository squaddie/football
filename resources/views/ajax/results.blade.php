<p>
    Match Results
</p>
@foreach($results->groupBy('week') as $result)
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th colspan="3">{{ $result->first()->week }}th Week Match Result</th>
        </tr>
        </thead>
        <tbody>
        @foreach($result as $group)
            <tr>
                <td>{{ $group->teams->first()->team }}</td>
                <td>
                    <b>
                        {{ $group->getTeamGoals($group->teams->first()->id) }}
                        -
                        {{ $group->getTeamGoals($group->teams->last()->id) }}
                    </b>
                </td>
                <td>{{ $group->teams->last()->team }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endforeach
