<p>
    @if($week <= 3)
        {{ $week }}th Week Prediction of Championship
    @else
        Final Weeks Prediction of Championship
    @endif
</p>
<table class="table table-hover table-bordered">
    <tbody>
    @foreach($teams->prediction() as $prediction)
        <tr>
            <td>{{ $prediction['team'] }}</td>
            <td>{{ $prediction['percent'] }} %</td>
        </tr>
    @endforeach
    </tbody>

</table>
