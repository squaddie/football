@extends('layouts.main')
@section('content')
    <section class="container mt-5">
        <div class="row">
            <div class="col-4">
                <p>
                    League table
                </p>
                <div id="league-render">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Teams</th>
                            <th>PTS</th>
                            <th>P</th>
                            <th>W</th>
                            <th>D</th>
                            <th>L</th>
                            <th>GD</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($league as $team)
                            @foreach($team->teams as $data)
                                <tr>
                                    <td>{{ $data->team }}</td>
                                    <td>{{ mt_rand(0,15) }}</td>
                                    <td>{{ mt_rand(0,15) }}</td>
                                    <td>{{ mt_rand(0,15) }}</td>
                                    <td>{{ mt_rand(0,15) }}</td>
                                    <td>{{ mt_rand(0,15) }}</td>
                                    <td>{{ mt_rand(0,15) }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                        <tr>
                            <td colspan="4">
                                <button class="play-all" type="button">Play all</button>
                            </td>
                            <td colspan="3">
                                <button class="play" type="button">Next week</button>
                            </td>
                        </tr>
                        </tbody>

                    </table>
                </div>
            </div>
            <div class="col-4">
                <div id="results-render"></div>
            </div>
            <div class="col-4">
                <div id="prediction-render"></div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function () {
            sessionStorage.removeItem('playWeek');
            sessionStorage.removeItem('openWeek');

            function renderResultsTable(week) {
                $.ajax({
                    method: 'GET',
                    url: '{{ route('get_result_table') }}',
                    data: {'week': week},
                    success: function (table) {
                        $('#results-render').html(table);
                    },
                    error: function () {

                    }
                });
            }

            function renderLeagueTable(openWeek) {
                $.ajax({
                    method: 'GET',
                    url: '{{ route('get_league_table') }}',
                    data: {'open_week': openWeek},
                    success: function (table) {
                        $('#league-render').html(table);
                    },
                    error: function () {

                    }
                });
            }

            function renderPredictionTable(week) {
                $.ajax({
                    method: 'GET',
                    url: '{{ route('get_prediction_table') }}',
                    data: {'week': week},
                    success: function (table) {
                        $('#prediction-render').html(table);
                    },
                    error: function () {

                    }
                });
            }

            function loopTrigger() {
                setInterval(function() {
                    $(document).find('.play').click().delay(1000);
                }, 1000);
            }

            $('.play-all').on('click', function () {
                let conf = confirm('Are you sure?');

                for (var i = 0; i < 4; i++) {
                    loopTrigger();
                }
            })

            $(document).on('click', '.play', function () {
                let playWeek = 1;
                let openWeek = 2;

                if (sessionStorage.getItem('playWeek'))
                    playWeek = sessionStorage.getItem('playWeek');

                if (sessionStorage.getItem('openWeek'))
                    openWeek = sessionStorage.getItem('openWeek');

                $.ajax({
                    method: 'GET',
                    url: '{{ route('play') }}',
                    data: {'play_week': playWeek, 'open_week': openWeek},
                    success: function (response) {
                        sessionStorage.setItem('playWeek', response['play_week']);
                        sessionStorage.setItem('openWeek', response['open_week'])
                        if (openWeek <= 5) {
                            renderLeagueTable(openWeek);
                            renderPredictionTable(openWeek);
                            renderResultsTable(openWeek);
                        }
                    },
                    error: function () {

                    }
                });
            });
        });
    </script>
@endsection
