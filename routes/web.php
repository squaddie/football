<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\ControllerHome::class, 'index'])->name('home');
Route::get('/play', [\App\Http\Controllers\ControllerPlay::class, 'play'])->name('play');
Route::get('/get-league-table', [\App\Http\Controllers\ControllerPlay::class, 'getLeagueTable'])->name('get_league_table');
Route::get('/get-results-table', [\App\Http\Controllers\ControllerPlay::class, 'getResultTable'])->name('get_result_table');
Route::get('/get-prediction-table', [\App\Http\Controllers\ControllerPlay::class, 'getPredictionTable'])->name('get_prediction_table');
Route::get('/parse', [\App\Http\Controllers\ControllerParser::class, 'index'])->name('parse');
